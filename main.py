from pylib.education_api import EducationAPI
from pylib.hasher import Hasher


if __name__ == "__main__":
    api = EducationAPI(login="teacher", password="students")
    hasher = Hasher()
    data = api.filter(1)
    for item in data:
        print(hasher.encode(item))
    print("Finish!")
