from typing import Dict, List, Any
import requests


class EducationAPI:
    url: str = "https://secrets-api.appbrewery.com/"

    def __init__(self, login: str, password: str, api_key: str = ""):
        self.session: requests.Session = self._create_session()
        self.login: str = login
        self.password: str = password
        self.api_key: str = api_key

    @staticmethod
    def _create_session() -> requests.Session:
        session = requests.Session()
        return session

    def register(self) -> requests.Response:
        response = requests.post(self.url + "register", data={"login": self.login, "password": self.password})
        if not response.ok:
            raise Exception(response.text)
        return response

    def generate_api_key(self) -> requests.Response:
        response = requests.post(self.url + "generate-api-key", data={"login": self.login, "password": self.password})
        if not response.ok:
            raise Exception(response.text)
        self.api_key = response.json()["apiKey"]
        return response

    def filter(self, score: int) -> List[Dict[str, Any]]:
        response = requests.get(self.url + "filter", data={"apiKey": self.api_key, "score": f"{score}"})
        return response.json()

