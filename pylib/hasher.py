from typing import Any
from hashlib import md5


class Hasher:

    @staticmethod
    def encode(data: Any) -> str:
        return md5(bytearray(data)).hexdigest()

