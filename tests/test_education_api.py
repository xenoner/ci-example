import requests_mock
from pylib.education_api import EducationAPI


url: str = "https://secrets-api.appbrewery.com/"


def test_register():
    api = EducationAPI(login="test", password="test")
    with requests_mock.Mocker() as response_mocker:
        response_mocker.register_uri("POST", url+"register", json={"success": "Successfully registered."})
        response = api.register()
    assert response.ok
    assert response.json()["success"] == "Successfully registered."
